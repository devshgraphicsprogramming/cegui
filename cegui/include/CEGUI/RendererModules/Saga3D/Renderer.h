/***********************************************************************
    created:    Thu Dec 27 2018
    author:     Manh Nguyen Tien
*************************************************************************/
/***************************************************************************
 *   Copyright (C) 2004 - 2011 Paul D Turner & The CEGUI Development Team
 *
 *   Permission is hereby granted, free of charge, to any person obtaining
 *   a copy of this software and associated documentation files (the
 *   "Software"), to deal in the Software without restriction, including
 *   without limitation the rights to use, copy, modify, merge, publish,
 *   distribute, sublicense, and/or sell copies of the Software, and to
 *   permit persons to whom the Software is furnished to do so, subject to
 *   the following conditions:
 *
 *   The above copyright notice and this permission notice shall be
 *   included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 *   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 *   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *   OTHER DEALINGS IN THE SOFTWARE.
 ***************************************************************************/
#ifndef _CEGUISaga3DRenderer_h_
#define _CEGUISaga3DRenderer_h_

#include "../../Renderer.h"
#include "../../Size.h"
#include "../../Vector.h"
#include "../../Rect.h"
#include <Saga.h>

#include <vector>
#include <map>

#if (defined( __WIN32__ ) || defined( _WIN32 )) && !defined(CEGUI_STATIC)
#   ifdef CEGUISAGA3DRENDERER_EXPORTS
#       define SAGA3D_GUIRENDERER_API __declspec(dllexport)
#   else
#       define SAGA3D_GUIRENDERER_API __declspec(dllimport)
#   endif
#else
#   define SAGA3D_GUIRENDERER_API
#endif

#if defined(_MSC_VER)
#   pragma warning(push)
#   pragma warning(disable : 4251)
#endif

// Start of CEGUI namespace section
namespace CEGUI
{
class Saga3DGeometryBuffer;
class Saga3DTexture;
class Saga3DImageCodec;

//! CEGUI::Renderer implementation for no particular engine
class SAGA3D_GUIRENDERER_API Saga3DRenderer : public Renderer
{
public:
    /*!
    \brief
        Convenience function that creates all the necessary objects
        then initialises the CEGUI system with them.

        This will create and initialise the following objects for you:
        - CEGUI::Saga3DRenderer
        - CEGUI::DefaultResourceProvider
        - CEGUI::System

    \param abi
        This must be set to CEGUI_VERSION_ABI

    \return
        Reference to the CEGUI::Saga3DRenderer object that was created.

    */
    static Saga3DRenderer& bootstrapSystem(saga::SagaDevice& device, const int abi = CEGUI_VERSION_ABI);
    
    /*!
    \brief
        Convenience function to cleanup the CEGUI system and related objects
        that were created by calling the bootstrapSystem function.

        This function will destroy the following objects for you:
        - CEGUI::System
        - CEGUI::DefaultResourceProvider
        - CEGUI::Saga3DRenderer

    \note
        If you did not initialise CEGUI by calling the bootstrapSystem function,
        you should \e not call this, but rather delete any objects you created
        manually.
    */
    static void destroySystem();

    /*!
    \brief
        Create an Saga3DRenderer object 
    */
    static Saga3DRenderer& create(saga::SagaDevice& device, const int abi = CEGUI_VERSION_ABI);

    //! destory an Saga3DRenderer object.
    static void destroy(Saga3DRenderer& renderer);

    //! function to create a CEGUI::Saga3DImageCodec object.
    static Saga3DImageCodec& createSaga3DImageCodec(saga::video::IVideoDriver& driver);

    auto& getDevice() { return d_device; }
    auto getUniform() const { return d_uniform; }
    auto getRenderPass() const { return d_pass; }
    auto getPipeline() const { return d_pipeline; }
    void present();
    void bindColorAttachment(const saga::video::TextureHandle t);
    void setActiveRenderTarget(RenderTarget* t);
    void setViewProjectionMatrix(const glm::mat4& m) { d_viewProjectionMatrix = m; }
    const auto& getViewProjectionMatrix() const { return d_viewProjectionMatrix; }
    const Rectf& getActiveViewport() const;

    /*!
    \brief
        Returns if the texture coordinate system is vertically flipped or not. The original of a
        texture coordinate system is typically located either at the the top-left or the bottom-left.
        CEGUI, Direct3D and most rendering engines assume it to be on the top-left. OpenGL assumes it to
        be at the bottom left.        
 
        This function is intended to be used when generating geometry for rendering the TextureTarget
        onto another surface. It is also intended to be used when trying to use a custom texture (RTT)
        inside CEGUI using the Image class, in order to determine the Image coordinates correctly.

    \return
        - true if flipping is required: the texture coordinate origin is at the bottom left
        - false if flipping is not required: the texture coordinate origin is at the top left
    */
    bool isTexCoordSystemFlipped() const { return false; }

    // implement CEGUI::Renderer interface
    RenderTarget& getDefaultRenderTarget();
    GeometryBuffer& createGeometryBuffer();
    void destroyGeometryBuffer(const GeometryBuffer& buffer);
    void destroyAllGeometryBuffers();
    TextureTarget* createTextureTarget();
    void destroyTextureTarget(TextureTarget* target);
    void destroyAllTextureTargets();
    Texture& createTexture(const String& name);
    Texture& createTexture(const String& name,
                           const String& filename,
                           const String& resourceGroup);
    Texture& createTexture(const String& name, const Sizef& size);
    void destroyTexture(Texture& texture);
    void destroyTexture(const String& name);
    void destroyAllTextures();
    Texture& getTexture(const String& name) const;
    bool isTextureDefined(const String& name) const;
    void beginRendering();
    void endRendering();
    void setDisplaySize(const Sizef& sz);
    const Sizef& getDisplaySize() const;
    const Vector2f& getDisplayDPI() const;
    uint getMaxTextureSize() const;
    const String& getIdentifierString() const;

protected:
    //! default constructor.
    Saga3DRenderer(saga::SagaDevice& device);
	//! common construction things.
	void constructor_impl();
    //! destructor.
    virtual ~Saga3DRenderer();

    //! helper to throw exception if name is already used.
    void throwIfNameExists(const String& name) const;
    //! helper to safely log the creation of a named texture
    static void logTextureCreation(const String& name);
    //! helper to safely log the destruction of a named texture
    static void logTextureDestruction(const String& name);

    //! String holding the renderer identification text.
    static String d_rendererID;
    //! The SagaDevide that we'll be using.
    saga::SagaDevice& d_device;
    //! Saga's video driver (as obtained from the device)
    saga::video::IVideoDriver& d_driver;
    //! Uniform handle for geometry buffer's transform
    saga::video::ShaderUniformHandle d_uniform;
    //! Render pass info
    saga::video::SRenderPass d_passInfo;
    //! Render pass handle
    saga::video::RenderPassHandle d_pass;
    //! Pipeline that renders geometry's material
    saga::video::PipelineHandle d_pipeline;
    //! Texture target storing screen contents
    saga::video::TextureHandle d_screen;
    //! Texture target for color attachment
    saga::video::TextureHandle d_colorAttachment;
    //! Texture target for depth attachment
    saga::video::TextureHandle d_depthAttachment;
    //! What the renderer considers to be the current display size.
    Sizef d_displaySize;
    //! What the renderer considers to be the current display DPI resolution.
    Vector2f d_displayDPI;
    //! The default RenderTarget
    RenderTarget* d_defaultTarget;
    //! The active RenderTarget
    RenderTarget* d_activeRenderTarget;
    //! View projection matrix
    glm::mat4 d_viewProjectionMatrix;
    //! container type used to hold TextureTargets we create.
    typedef std::vector<TextureTarget*> TextureTargetList;
    //! Container used to track texture targets.
    TextureTargetList d_textureTargets;
    //! container type used to hold GeometryBuffers we create.
    typedef std::vector<Saga3DGeometryBuffer*> GeometryBufferList;
    //! Container used to track geometry buffers.
    GeometryBufferList d_geometryBuffers;
    //! container type used to hold Textures we create.
    typedef std::map<String, Saga3DTexture*, StringFastLessCompare
                     CEGUI_MAP_ALLOC(String, Saga3DTexture*)> TextureMap;
    //! Container used to track textures.
    TextureMap d_textures;
    //! What the renderer thinks the max texture size is.
    uint d_maxTextureSize;
};


} // End of  CEGUI namespace section

#if defined(_MSC_VER)
#   pragma warning(pop)
#endif

#endif  // end of guard _CEGUISaga3DRenderer_h_
