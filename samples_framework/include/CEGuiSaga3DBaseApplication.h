/***********************************************************************
    created:    Fri Dec 28 2018
    author:     Manh Nguyen Tien
*************************************************************************/
/***************************************************************************
 *   Copyright (C) 2004 - 2009 Paul D Turner & The CEGUI Development Team
 *
 *   Permission is hereby granted, free of charge, to any person obtaining
 *   a copy of this software and associated documentation files (the
 *   "Software"), to deal in the Software without restriction, including
 *   without limitation the rights to use, copy, modify, merge, publish,
 *   distribute, sublicense, and/or sell copies of the Software, and to
 *   permit persons to whom the Software is furnished to do so, subject to
 *   the following conditions:
 *
 *   The above copyright notice and this permission notice shall be
 *   included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 *   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 *   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *   OTHER DEALINGS IN THE SOFTWARE.
 ***************************************************************************/
#ifndef _CEGuiSaga3DBaseApplication_h_
#define _CEGuiSaga3DBaseApplication_h_

#if defined(_WIN32)
#  pragma comment(lib, "Saga3D.lib")
#if !defined(CEGUI_STATIC)
#  if defined(_DEBUG)
#      pragma comment(lib, "CEGUISaga3DRenderer_d.lib")
#  else
#      pragma comment(lib, "CEGUISaga3DRenderer.lib")
#  endif
#else
# define _IRR_STATIC_LIB_ //Define this regardless if we are using a dll or not
# if defined(_DEBUG)
#  pragma comment(lib, "CEGUISaga3DRenderer_Static_d.lib")
# else
#  pragma comment(lib, "CEGUISaga3DRenderer_Static.lib")
# endif
#endif
#endif

#include "CEGuiBaseApplication.h"
#include "CEGUI/Size.h"
#include <Saga.h>

namespace CEGUI
{
    class Saga3DEventPusher;
}

class CEGuiSaga3DBaseApplication : public CEGuiBaseApplication,
                                     public saga::IEventReceiver
{
public:
    CEGuiSaga3DBaseApplication();
    ~CEGuiSaga3DBaseApplication();
    void destroyRenderer();

    // Saga3D event listener
    void onEvent(const SDL_Event& event) override;

protected:
    /// member to check and handle resizing of the display window.
    void checkWindowResize();

    // implementation of base class abstract methods.
    void run() override;
    void destroyWindow() override;
    void beginRendering(const float elapsed) override;
    void endRendering() override;

    void processEvent(const SDL_Event& event);

    // bool OnKeyDown(saga::EKEY_CODE key, wchar_t wch, bool /*ctrl*/, bool /*shift*/);
    // bool OnKeyUp(saga::EKEY_CODE key, wchar_t /*wch*/, bool /*ctrl*/, bool /*shift*/);
    // bool OnMouse(std::int32_t x, std::int32_t y, saga::f32 w, saga::EMOUSE_INPUT_EVENT e);

    std::unique_ptr<saga::SagaDevice>           d_device = nullptr;
    std::shared_ptr<saga::video::IVideoDriver>  d_driver = nullptr;
    std::shared_ptr<saga::scene::ISceneManager> d_smgr   = nullptr;
    std::uint32_t                               d_lastTime = 0;

    // const CEGUI::Saga3DEventPusher* d_eventPusher;

    //! size of display last time a change was detected.
    CEGUI::Sizef d_lastDisplaySize;
};


#endif  // end of guard _CEGuiSaga3DBaseApplication_h_
