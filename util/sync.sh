cd ..
find . -mindepth 1 -maxdepth 1 -type d -not -name util -exec rm -rf '{}' \;
find . -mindepth 1 -maxdepth 1 -type f -exec rm -rf '{}' \;
git init .
../hg_export/hg-fast-export.sh -r ../cegui
git checkout saga
cp util/.gitignore .
git add .
git commit -m "Add utility files"
git remote add origin git@gitlab.com:InnerPieceOSS/cegui.git
git push origin saga -f


