################################################################################
# Custom cmake module for CEGUI to find the Saga3D engine
################################################################################
include(FindPackageHandleStandardArgs)

find_path(SAGA3D_H_PATH NAMES Saga.h PATH_SUFFIXES saga)
find_library(SAGA3D_LIB NAMES saga libsaga)
find_library(SAGA3D_LIB_DBG NAMES saga libsaga)
mark_as_advanced(SAGA3D_H_PATH SAGA3D_LIB SAGA3D_LIB_DBG)

find_package_handle_standard_args(SAGA3D DEFAULT_MSG SAGA3D_LIB SAGA3D_H_PATH)

# set up output vars
if (SAGA3D_FOUND)
    set (SAGA3D_INCLUDE_DIR ${SAGA3D_H_PATH})
    set (SAGA3D_LIBRARIES ${SAGA3D_LIB} SDL2 assimp vulkan ${SAGA3D_H_PATH}/../bin/libVEZ.so)
    if (SAGA3D_LIB_DBG)
        set (SAGA3D_LIBRARIES_DBG ${SAGA3D_LIB_DBG})
    endif()
else()
    set (SAGA3D_INCLUDE_DIR)
    set (SAGA3D_LIBRARIES)
    set (SAGA3D_LIBRARIES_DBG)
endif()

